# Synapse

A easy-to-use Deep Neural Network Engine

# License

<details>

  <summary> Copyright (C) 2019  Syed Hasibur Rahman </summary>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

</details>


# Author
__Syed Hasibur Rahman__<br>
_Deep Learning Engineer_<br>
_Leapmind Inc._<br> 